
.. currentmodule:: tango

.. _contents: 

========
Contents
========

.. toctree::
    :maxdepth: 2
    :titlesonly:

    start
    quicktour
    ITango <itango>
    green_modes/green
    API <api>
    How to <howto>
    How to contribute <how-to-contribute>
    How to test <testing>
    Python version policy <version-policy>
    FAQ <faq>
    TEP <tep>
    What's new? <news>
    indexes
    
**Last update:** |today|

